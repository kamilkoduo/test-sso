#!/usr/bin/env bash

docker build -t registry.gitlab.com/kamilkoduo/test-sso/symfony -f symfony-app/docker/buildpush.sh .
docker push registry.gitlab.com/kamilkoduo/test-sso/symfony
